# rabbitmq笔记

## 安装 

官网地址 https://www.rabbitmq.com/download.html

### 文件上传

![image-20210731150952283](image/image-20210731150952283.png)

### 安装文件(分别按照以下顺序安装)

rpm -ivh erlang-21.3-1.el7.x86_64.rpm

yum install socat -y

rpm -ivh rabbitmq-server-3.8.8-1.el7.noarch.rpm

> 常用命令(按照以下顺序执行) 添加开机启动 RabbitMQ 服务

 chkconfig rabbitmq-server on 

> 启动服务 

/sbin/service rabbitmq-server start

> 查看服务状态

 /sbin/service rabbitmq-server status

> 停止服务(选择执行) 

/sbin/service rabbitmq-server stop

> 开启 web 管理插件

 rabbitmq-plugins enable rabbitmq_management 

用默认账号密码(guest)访问地址 http://ip:15672/出现权限问题

### 添加一个新的用户 

> 创建账号

rabbitmqctl add_user admin 123 

> 设置用户角色

 rabbitmqctl set_user_tags admin administrator 

> 设置用户权限

```
rabbitmqctl set_permissions -p "/" admin ".*" ".*" ".*"
```

用户 user_admin 具有/vhost1 这个 virtual host 中所有资源的配置、写、读权限 

> 当前用户和角色 的查询

rabbitmqctl list_users

## 简单模式

### 生产者

```java
package com.lq.rabbitmq.deom01;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 生产者
 */
public class Producer {
    /**
     * 消息队列名
     */
    public static final String QUEUE_NAME = "hello";

    //发信息
    public static void main(String[] args) throws IOException, TimeoutException {
        //创建工厂连接
        ConnectionFactory factory = new ConnectionFactory();
        //设置地址
        factory.setHost("81.68.139.38");
        //设置用户名和密码
        factory.setUsername("admin");
        factory.setPassword("123");
        //连接
        Connection connection = factory.newConnection();
        //获取信道
        Channel channel = connection.createChannel();
        //生产队列
        /**
         *  参数一  队列名
         *  参数二   队列信息是否持久化 默认false
         *  参数三    改队列消息是否共享  默认 false 不共享
         *  参数四3   改队列是否自动删除 false
         */
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        //发送消息
        String msg = "hello world";
        /**
         *  参数一  交换机名
         *  参数二  路由的key值
         *  参数三  其他
         *  参数四 发送消息的信息体
         */
        channel.basicPublish("", QUEUE_NAME, null, msg.getBytes());
        System.out.println("信息加入队列成功!");
    }
}
```

### 消费者

```java
package com.lq.rabbitmq.deom01;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消费者
 */
public class Consumer {
    /**
     * 消息队列名
     */
    public static final String QUEUE_NAME = "hello";

    //消费者
    public static void main(String[] args) throws IOException, TimeoutException {
        //创建工厂连接
        ConnectionFactory factory = new ConnectionFactory();
        //设置地址
        factory.setHost("81.68.139.38");
        //设置用户名和密码
        factory.setUsername("admin");
        factory.setPassword("123");
        //连接
        Connection connection = factory.newConnection();
        //获取信道
        Channel channel = connection.createChannel();
        //获取信道的信息
      //声明
        DeliverCallback deliverCallback=(consumerTag,message)->{
            System.out.println(new String(message.getBody()));
        };
        //取消信息时的回调
        CancelCallback cancelCallback=consunmeTag->{
            System.out.println("消费取消");
        };
        /**
         *  参数一   消费队列名
         *  参数二   消费成功是否自动应答
         *  参数三     消费者消费失败的回调
         *  参数四     消费者取消消费的回调
         */
        channel.basicConsume(QUEUE_NAME,true,deliverCallback,cancelCallback);
    }
}
```



