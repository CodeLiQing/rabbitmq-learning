package com.lq.rabbitmq.deom08;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

import java.io.IOException;

public class Diret {

    private static final String CHANGE_NAME="change_name";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtile.createConnection();
        //死信 设置ttl时间
/*        AMQP.BasicProperties properties = new AMQP
                .BasicProperties()
                .builder().expiration("10000").build();*/
        for (int i = 1; i <11 ; i++) {
            String msg="info"+i;
            channel.basicPublish(CHANGE_NAME,"zhangsan",null,msg.getBytes());
        }
    }
}
