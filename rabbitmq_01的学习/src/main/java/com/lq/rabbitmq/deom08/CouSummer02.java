package com.lq.rabbitmq.deom08;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *  死信队列
 *   消费者
 */
public class CouSummer02 {
    private static final String QUEUE_DEAD="dead_name";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtile.createConnection();

        DeliverCallback deliverCallback=(Tag,message)->{
            System.out.println("接收信息:"+new String(message.getBody(),"utf-8"));
        };

        channel.basicConsume(QUEUE_DEAD,true,deliverCallback,(counser)->{});
    }
}
