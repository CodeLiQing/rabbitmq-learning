package com.lq.rabbitmq.deom05;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;

/**
 * 订阅模式的接收  消费者
 */
public class FanoutCouSummer01 {
    private static String CHANGE_NAME="logs";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtile.createConnection();
        //创建交换机声明
        channel.exchangeDeclare(CHANGE_NAME,"fanout");
        //创建一个队列 临时
        String queue = channel.queueDeclare().getQueue();
        //绑定
        /**
         *  参数一  队列名
         *  参数二   交换机
         *  参数三  路由key
         */
        channel.queueBind(queue,CHANGE_NAME,"");
        System.out.println("FanoutCouSummer01>信道与交换机和队列绑定成功,等待接收信息中...");
        DeliverCallback deliverCallback=(counMer,message)->{
            System.out.println("接收到信息："+new String(message.getBody()));
        };
        CancelCallback cancelCallback=(coumer)->{
            System.out.println("取消接收");
        };
        channel.basicConsume(queue,true,deliverCallback,cancelCallback);
    }

}
