package com.lq.rabbitmq.deom05;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.util.Scanner;

/**
 * 发送消息
 */
public class Emit {
    private static String CHANGE_NAME="logs";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtile.createConnection();
        //创建交换机声明
        channel.exchangeDeclare(CHANGE_NAME,"fanout");
        //从控制台中接收
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String s = scanner.next();
            channel.basicPublish(CHANGE_NAME,"",null,s.getBytes("utf-8"));
            System.out.println("信息发送成功！:"+s);
        }

    }
}
