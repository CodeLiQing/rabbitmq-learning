package com.lq.rabbitmq.deom03;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.lq.rabbitmq.utile.SleepUtile;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;

/**
 * 消费者  信息改成手动应答 信息不丢失 信息重新入队
 */
public class Work04 {
    private static final String TASK_NAME="ack_queue";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtile.createConnection();
        System.out.println("c2时间长");
        DeliverCallback deliverCallback=(coumTag,message)->{
            //沉睡1秒
            SleepUtile.sleep(20);
            System.out.println("信息接收:"+new String(message.getBody(),"utf-8"));
            //手动应答 信息标记    不批量处理应答
            channel.basicAck(message.getEnvelope().getDeliveryTag(),false);
        };
        CancelCallback cancelCallback=(consTag)->{
            System.out.println("取消接收信息");
        };
        //设置不公平分发 默认是轮巡
        channel.basicQos(1);
        channel.basicConsume(TASK_NAME,false,deliverCallback,cancelCallback);
    }
}
