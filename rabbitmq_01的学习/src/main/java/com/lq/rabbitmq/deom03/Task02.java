package com.lq.rabbitmq.deom03;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.Scanner;

/**
 *   信息改成手动应答 信息不丢失 信息重新入队
 */
public class Task02 {
    private static final String TASK_NAME="ack_queue";
    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtile.createConnection();
        //持久化
        boolean dur=true;
       channel.queueDeclare(TASK_NAME,dur,false,false,null);
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String next = scanner.next();
            //设置生产者发送的信息持久化  保存在磁盘上  不能完全保证绝对成功存储
            channel.basicPublish("",TASK_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN,next.getBytes("UTF-8"));
            System.out.println("信息发送成功:"+next);
        }
    }
}
