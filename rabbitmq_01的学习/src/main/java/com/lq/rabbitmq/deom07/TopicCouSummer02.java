package com.lq.rabbitmq.deom07;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

import java.io.IOException;

/**
 *  主题交换机   消费者
 */
public class TopicCouSummer02 {
  private static String CHANGE_NAME="topic_logs";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtile.createConnection();
        //声明交换机
        channel.exchangeDeclare(CHANGE_NAME, BuiltinExchangeType.TOPIC);
       //创建队列
        String queueName="Q2";
        //声明队列
        channel.queueDeclare(queueName,false,false,false,null);
        //绑定
        channel.queueBind(queueName,CHANGE_NAME,"*.*.rabbit");
        channel.queueBind(queueName,CHANGE_NAME,"lazy.#");
        System.out.println("TTopicCouSummer02等待接收信息...");

        //接收信息
        channel.basicConsume(queueName,true,(Tag,message)->{
            System.out.println(" TopicCouSummer02,接收信息:"+new String(message.getBody(),"utf-8"));
        },(consummer)->{});
    }
}
