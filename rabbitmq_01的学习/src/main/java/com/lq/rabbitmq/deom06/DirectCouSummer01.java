package com.lq.rabbitmq.deom06;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;

/**
 *
 */
public class DirectCouSummer01 {
     private static String DIR_NAME="direct_logs";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtile.createConnection();
        //创建交换机声明
        channel.exchangeDeclare(DIR_NAME, BuiltinExchangeType.DIRECT);
        //创建一个队列 临时
        channel.queueDeclare("console",false,false,false,null);
        //绑定
        /**
         *  参数一  队列名
         *  参数二   交换机
         *  参数三  路由key
         */
        channel.queueBind("console",DIR_NAME,"info");
        channel.queueBind("console",DIR_NAME,"warning");
        System.out.println("DirectCouSummer01>信道与交换机和队列绑定成功,等待接收信息中...");
        DeliverCallback deliverCallback=(counMer, message)->{
            System.out.println("接收到信息："+new String(message.getBody()));
        };
        CancelCallback cancelCallback=(coumer)->{
            System.out.println("取消接收");
        };
        channel.basicConsume("console",true,deliverCallback,cancelCallback);
    }
}
