package com.lq.rabbitmq.deom06;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.util.Scanner;

/**
 * 发送消息 直接交换机   通过 路由key来 分发信息
 */
public class DirectEmit {
    private static String DIR_NAME="direct_logs";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtile.createConnection();
        //创建交换机声明
        channel.exchangeDeclare(DIR_NAME, BuiltinExchangeType.DIRECT);
        //从控制台中接收
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String s = scanner.next();
            channel.basicPublish(DIR_NAME,"error",null,s.getBytes("utf-8"));
            System.out.println("信息发送成功！:"+s);
        }

    }
}
