package com.lq.rabbitmq.deom02;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.util.Scanner;

/**
 * 生产者  大量的信息
 */
public class Task01 {
       public static  final String QUEUE_NAME="hello";

    public static void main(String[] args) throws IOException {
        Channel channel = RabbitmqUtile.createConnection();
        //生产队列
        /**
         *  参数一  队列名
         *  参数二   队列信息是否持久化 默认false
         *  参数三    改队列消息是否共享  默认 false 不共享
         *  参数四3   改队列是否自动删除 false
         */
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        //从控制台中接收
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String s = scanner.next();
            channel.basicPublish("",QUEUE_NAME,null,s.getBytes());
            System.out.println("信息发送成功！");
        }
    }
}
