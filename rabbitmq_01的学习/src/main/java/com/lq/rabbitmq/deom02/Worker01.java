package com.lq.rabbitmq.deom02;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;

import javax.management.QueryEval;
import java.io.IOException;

/**
 *  工作线程 （相当于原来的消费者）
 */
public class Worker01 {
    private static final String QUEUE_NAME="hello";
    public static void main(String[] args) throws IOException {
         //获取信道
        Channel channel = RabbitmqUtile.createConnection();
        //信息接收

        //成功接收信息:
        DeliverCallback deliverCallback=(constag,mesagge)->{
            System.out.println("接收到的信息："+new String(mesagge.getBody()));
        };
        //信息接收被取消
        CancelCallback cancelCallback=(consumeTag)->{
            System.out.println("取消接收信息");
        };

        /**
         *  参数一   消费队列名
         *  参数二   消费成功是否自动应答
         *  参数三     消费者消费失败的回调
         *  参数四     消费者取消消费的回调
         */
        System.out.println("C2等待线程...");
        channel.basicConsume(QUEUE_NAME,true,deliverCallback,cancelCallback);
    }
}
