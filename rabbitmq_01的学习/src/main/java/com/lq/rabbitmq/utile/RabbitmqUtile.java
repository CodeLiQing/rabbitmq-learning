package com.lq.rabbitmq.utile;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitmqUtile {

    /**
     *  获取信道
     * @return
     */
    public static Channel createConnection(){
        //创建工厂连接
        ConnectionFactory factory = new ConnectionFactory();
        //设置地址
        factory.setHost("81.68.139.38");
        //设置用户名和密码
        factory.setUsername("admin");
        factory.setPassword("123");
        Connection connection=null;
        Channel channel=null;
        //连接

            try {
                connection = factory.newConnection();
                //获取信道
                 channel = connection.createChannel();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }
        return   channel;
    }
}
