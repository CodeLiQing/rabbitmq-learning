package com.lq.rabbitmq.deom01;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * 生产者
 */
public class Producer {
    /**
     * 消息队列名
     */
    public static final String QUEUE_NAME = "hello";

    //发信息
    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitmqUtile.createConnection();
        //生产队列
        /**
         *  参数一  队列名
         *  参数二   队列信息是否持久化 默认false
         *  参数三    改队列消息是否共享  默认 false 不共享
         *  参数四3   改队列是否自动删除 false
         */
        Map<String, Object> map =new HashMap<>();
        //设置最大优先级范围 10
        map.put("x-max-priority",10);
        channel.queueDeclare(QUEUE_NAME, false, false, false, map);
        //发送消息
        for (int i = 1; i <11 ; i++) {
             String msg="info:"+i;
            AMQP.BasicProperties basicProperties=null;
             if(i==5){
                  //设置权限优先级
                 basicProperties=new AMQP.BasicProperties()
                         .builder().priority(5).build();
             }
            /**
             *  参数一  交换机名
             *  参数二  路由的key值
             *  参数三  其他
             *  参数四 发送消息的信息体
             */
            channel.basicPublish("", QUEUE_NAME, basicProperties, msg.getBytes());

        }
        System.out.println("信息加入队列成功!");
    }
}
