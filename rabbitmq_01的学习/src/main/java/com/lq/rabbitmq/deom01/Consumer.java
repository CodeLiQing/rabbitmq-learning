package com.lq.rabbitmq.deom01;

import com.lq.rabbitmq.utile.RabbitmqUtile;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消费者
 */
public class Consumer {
    /**
     * 消息队列名
     */
    public static final String QUEUE_NAME = "hello";

    //消费者
    public static void main(String[] args) throws IOException, TimeoutException {
        Channel channel = RabbitmqUtile.createConnection();
        //获取信道的信息
        //声明
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            System.out.println(new String(message.getBody()));
        };
        //取消信息时的回调
        CancelCallback cancelCallback = consunmeTag -> {
            System.out.println("消费取消");
        };
        /**
         *  参数一   消费队列名
         *  参数二   消费成功是否自动应答
         *  参数三     消费者消费失败的回调
         *  参数四     消费者取消消费的回调
         */
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, cancelCallback);
    }
}
