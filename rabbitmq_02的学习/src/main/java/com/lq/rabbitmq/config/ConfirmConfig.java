package com.lq.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;

@Configuration
public class ConfirmConfig {
    public static final String EXCHANGE_NAME = "exchange_name";
    public static final String QUEUE_NAME = "queue_name";
    public static final String ROUTING_KEY = "routing_key";
    //备份交换机
    public static final String BACKUP_EXCHANGE_NAME = "backup_exchange";
    //备份队列
    public static final String BACKUP_QUEUE_NAME = "backup_queue";
    //报警队列
    public static final String BACKUP_WARNING_NAME = "warning_exchange";

    //交换机与备用交换机绑定
    @Bean("exchange_name")
    public DirectExchange exchange() {
        return ExchangeBuilder.directExchange(EXCHANGE_NAME).durable(true)
                .withArgument("alternate-exchange",BACKUP_EXCHANGE_NAME).build();
    }

    @Bean
    public Queue queueConfirm() {
        return QueueBuilder.durable(QUEUE_NAME).build();
    }

    @Bean
    public Binding confirm(@Qualifier("exchange_name") DirectExchange directExchange,
                           @Qualifier("queueConfirm") Queue queue) {
        return BindingBuilder.bind(queue).to(directExchange).with(ROUTING_KEY);
    }

    @Bean
    public Queue backupQueue() {
        return QueueBuilder.durable(BACKUP_QUEUE_NAME).build();
    }

    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange(BACKUP_EXCHANGE_NAME);
    }
    @Bean
    public Queue warningExchange() {
        return QueueBuilder.durable(BACKUP_WARNING_NAME).build();
    }

    //绑定
    @Bean
    public Binding BindingBackupQueue(@Qualifier("backupQueue") Queue backupQueue,
                                 @Qualifier("fanoutExchange") FanoutExchange fanoutExchange){
        return BindingBuilder.bind(backupQueue).to(fanoutExchange);
    }

    //绑定
    @Bean
    public Binding BindingWarningExchange(@Qualifier("fanoutExchange") FanoutExchange fanoutExchange,
                                 @Qualifier("warningExchange")Queue warningExchange){
        return BindingBuilder.bind(warningExchange).to(fanoutExchange);
    }
}
