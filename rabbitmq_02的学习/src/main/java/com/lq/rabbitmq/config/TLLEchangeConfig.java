package com.lq.rabbitmq.config;

import com.rabbitmq.client.AMQP;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 延迟队列
 */
@Configuration
public class TLLEchangeConfig {
    private static final String X_EXCHANGE = "X";
    private static final String DEAD_Y_EXCHANGE = "Y";
    private static final String QUEUE_A = "QA";
    private static final String QUEUE_B = "QB";
    private static final String DEAD_QUEUE = "QD";
    private static final String QUEUE_C="QC";


    @Bean("Exchange")
    public DirectExchange Exchange() {
        return new DirectExchange(X_EXCHANGE);
    }

    @Bean("deadExchange")
    public DirectExchange deadExchange() {
        return new DirectExchange(DEAD_Y_EXCHANGE);
    }
    @Bean("queueC")
    public Queue queueC() {
        Map<String, Object> map = new HashMap<>();
        //设置死信交换机
        map.put("x-dead-letter-exchange", DEAD_Y_EXCHANGE);
        //设置死信路由key
        map.put("x-dead-letter-routing-key", "YD");
        return QueueBuilder.durable(QUEUE_C).withArguments(map).build();
    }

    @Bean
    public Binding queueCqueueBing(@Qualifier("queueC")Queue queuec,
                          @Qualifier("Exchange")DirectExchange Exchange){
        return BindingBuilder.bind(queuec).to(Exchange).with("XC");
    }



    /**
     *  ttl  10秒
     * @return
     */
    @Bean("queueA")
    public Queue queueA() {
        Map<String, Object> map = new HashMap<>();
        //设置死信交换机
        map.put("x-dead-letter-exchange", DEAD_Y_EXCHANGE);
        //设置死信路由key
        map.put("x-dead-letter-routing-key", "YD");
        //设置过期时间
        map.put("x-message-ttl", 10 * 1000);
        return QueueBuilder.durable(QUEUE_A).withArguments(map).build();
    }

    /**
     * 40秒
     * @return
     */
    @Bean("queueB")
    public Queue queueB() {
        Map<String, Object> map = new HashMap<>();
        //设置死信交换机
        map.put("x-dead-letter-exchange", DEAD_Y_EXCHANGE);
        //设置死信路由key
        map.put("x-dead-letter-routing-key", "YD");
        //设置过期时间
        map.put("x-message-ttl", 40 * 1000);
        return QueueBuilder.durable(QUEUE_B).withArguments(map).build();
    }

    /**
     * 死信duil
     */
    @Bean("queueD")
    public Queue queueD() {
        return QueueBuilder.durable(DEAD_QUEUE).build();
    }

    //绑定
    @Bean
    public Binding queueAqueueBing(@Qualifier("queueA")Queue queueA,
                             @Qualifier("Exchange")DirectExchange exchange){
        return BindingBuilder.bind(queueA).to(exchange).with("XA");
    }

    //绑定
    @Bean
    public Binding queueBqueueBing(@Qualifier("queueB")Queue queueb,
                             @Qualifier("Exchange")DirectExchange exchange){
        return BindingBuilder.bind(queueb).to(exchange).with("XB");
    }
    //绑定
    @Bean
    public Binding YqueueBing(@Qualifier("queueD")Queue queueD,
                                   @Qualifier("deadExchange")DirectExchange deadExchange){
        return BindingBuilder.bind(queueD).to(deadExchange).with("YD");
    }
}
