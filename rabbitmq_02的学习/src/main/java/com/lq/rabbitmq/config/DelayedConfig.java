package com.lq.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 *  延迟队列     插件的消息队列使用
 */
@Configuration
public class DelayedConfig {

     public static final String CHANGE_NAME="changes.name";
    public static final String QUEUE_NAME="delayed.exchange";
    public static final String ROUTING_KEY="delayed.routing.key";

    /**
     *  创建队列bean
     * @return
     */
     @Bean
    public Queue delayedMame(){
         return new Queue(QUEUE_NAME);
     }

    /**
     *  创建交换机bean
     * @return
     */
     @Bean
    public CustomExchange customExchange(){
         Map<String, Object> arguments=new HashMap<>();
         arguments.put("x-delayed-type","direct");
         return new CustomExchange(CHANGE_NAME,"x-delayed-message"
                 ,true,false,arguments);
     }

    /**
     *  交换机和队列及路由的绑定
     * @param delayedMame
     * @param customExchange
     * @return
     */
     @Bean
      public Binding bindingDelayed(@Qualifier("delayedMame") Queue delayedMame,
                                    @Qualifier("customExchange")CustomExchange customExchange){
       return BindingBuilder.bind(delayedMame).to(customExchange).with(ROUTING_KEY).noargs();
     }
}
