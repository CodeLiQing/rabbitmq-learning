package com.lq.rabbitmq.controller;

import com.lq.rabbitmq.config.ConfirmConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@Slf4j
@RestController
public class ProductController {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    //开始发送消息
    @GetMapping("/product1/{s}")
    public void sendMsg(@PathVariable("s")String msg){
        log.info("现在开始发送消息,内容:"+msg);
        CorrelationData correlationData=new CorrelationData();
        correlationData.setId("1");
        rabbitTemplate.convertAndSend(ConfirmConfig.EXCHANGE_NAME,ConfirmConfig.ROUTING_KEY,msg,correlationData);
    }

    //开始发送消息
    @GetMapping("/product2/{s}")
    public void sendMsg2(@PathVariable("s")String msg){
        log.info("现在开始发送消息,内容:"+msg);
        CorrelationData correlationData=new CorrelationData();
        correlationData.setId("2");
        rabbitTemplate.convertAndSend(ConfirmConfig.EXCHANGE_NAME,ConfirmConfig.ROUTING_KEY+"000",msg,correlationData);
    }
}
