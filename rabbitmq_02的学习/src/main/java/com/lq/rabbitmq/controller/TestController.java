package com.lq.rabbitmq.controller;

import com.lq.rabbitmq.config.DelayedConfig;
import io.swagger.models.auth.In;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@Log4j2
@RestController
public class TestController {

    @Autowired
    private RabbitTemplate rabbitTemplate;
     //开始发送消息
    @GetMapping("/test/{s}")
    public void sendMsg(@PathVariable("s")String msg){
        log.info("当前时间:"+new Date());
        rabbitTemplate.convertAndSend("X","XA","信息来 自ttl为10秒队列:"+msg);
        rabbitTemplate.convertAndSend("X","XB","信息来 自ttl为40秒队列:"+msg);
    }
    //开始发送消息
    @GetMapping("/test/{s}/{ttl}")
    public void sdenMsgtLL(@PathVariable("s")String msg,@PathVariable("ttl") String ttl){
        log.info("当前时间:"+new Date()+","+ttl+"毫秒给信息队列QC");
       rabbitTemplate.convertAndSend("X","XC",msg,mssg->{
           //延迟信息的时长
             mssg.getMessageProperties().setExpiration(ttl);
           return mssg;
       });
    }

     //基于插件的发送消息
     @GetMapping("/delayed/{s}/{ttl}")
    public void delayed(@PathVariable("s")String msg,@PathVariable("ttl") Integer ttl) {
         log.info("当前时间:" + new Date() + "," + ttl + "毫秒给信息队列QC");
         rabbitTemplate.convertAndSend(DelayedConfig.CHANGE_NAME,DelayedConfig.ROUTING_KEY,msg, mssg->{
             //延迟信息的时长
             mssg.getMessageProperties().setDelay(ttl);
             return mssg;
         });
     }
}
