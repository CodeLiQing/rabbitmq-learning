package com.lq.rabbitmq.consummer;

import com.lq.rabbitmq.config.DelayedConfig;
import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class ConSummer {

     //接收信息
    @RabbitListener(queues = "QD")
    public void received(Message message, Channel channel){
        System.out.println("QD收到信息:"+new String(message.getBody()));
     //   log.info("QD收到信息:"+new String(message.getBody()));
    }

    @RabbitListener(queues = DelayedConfig.QUEUE_NAME)
    public void queues(Message message, Channel channel){
        System.out.println("DelayedConfig收到信息:"+new String(message.getBody()));
        //   log.info("QD收到信息:"+new String(message.getBody()));
    }

}
