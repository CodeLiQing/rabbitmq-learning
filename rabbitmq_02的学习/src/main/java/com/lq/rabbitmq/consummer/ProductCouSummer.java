package com.lq.rabbitmq.consummer;

import com.lq.rabbitmq.config.ConfirmConfig;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductCouSummer {

    @RabbitListener(queues = ConfirmConfig.QUEUE_NAME)
    public void  queue(Message message, Channel channel){
        System.out.println("接收到的信息:"+new String(message.getBody()));
    }

    @RabbitListener(queues = ConfirmConfig.BACKUP_WARNING_NAME)
    public void  ConfirmConfig(Message message, Channel channel){
        System.out.println("警告 !!1没有接收成功的信息:"+new String(message.getBody()));
    }

}
