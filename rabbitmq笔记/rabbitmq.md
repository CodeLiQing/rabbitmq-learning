## 安装 

官网地址 https://www.rabbitmq.com/download.html

### 文件上传

![image-20210731150952283](image/image-20210731150952283.png)

### 安装文件(分别按照以下顺序安装)

rpm -ivh erlang-21.3-1.el7.x86_64.rpm

yum install socat -y

rpm -ivh rabbitmq-server-3.8.8-1.el7.noarch.rpm

> 常用命令(按照以下顺序执行) 添加开机启动 RabbitMQ 服务

 chkconfig rabbitmq-server on 

> 启动服务 

/sbin/service rabbitmq-server start

> 查看服务状态

 /sbin/service rabbitmq-server status

> 停止服务(选择执行) 

/sbin/service rabbitmq-server stop

> 开启 web 管理插件

 rabbitmq-plugins enable rabbitmq_management 

用默认账号密码(guest)访问地址 http://ip:15672/出现权限问题

### 添加一个新的用户 

> 创建账号

rabbitmqctl add_user admin 123 

> 设置用户角色

 rabbitmqctl set_user_tags admin administrator 

> 设置用户权限

```
rabbitmqctl set_permissions -p "/" admin ".*" ".*" ".*"
```

用户 user_admin 具有/vhost1 这个 virtual host 中所有资源的配置、写、读权限 

> 当前用户和角色 的查询

rabbitmqctl list_users

# docker 安装rabbitmq

```liunx
拉取镜像：

docker pull rabbitmq:management
1
查看docker镜像列表：

docker images
1
Docker容器操作：
ok，上面命令执行后，镜像就已经拉取到本地仓库了，然后可以进行容器操作，启动rabbitMQ

简单版

docker run -d -p 5672:5672 -p 15672:15672 --name rabbitmq rabbitmq:management
1
-d 后台运行
-p 隐射端口
–name 指定rabbitMQ名称
复杂版（设置账户密码，hostname）

docker run -d -p 15672:15672  -p  5672:5672  -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin --name rabbitmq --hostname=rabbitmqhostone  rabbitmq:management
1
-d 后台运行
-p 隐射端口
–name 指定rabbitMQ名称
RABBITMQ_DEFAULT_USER 指定用户账号
RABBITMQ_DEFAULT_PASS 指定账号密码
执行如上命令后访问：http://ip:15672/

默认账号密码：guest/guest



#######补充
docker search rabbitmq-management，不带management的版本可能没有管理界面，也就是无法通过web登录后台系统

2. docker pull rabbitmq-management，或者写的更加明确，如docker pull docker.io/macintoshplus/rabbitmq-management:latest，默认的标签也是latest版本。也可以去docker hub上选择自己需要的版本search一下，查的出来就能够pull下来。

执行docker run -d -p 15672:15672 -p 5672:5672 --name rabbitmq --hostname=rabbitmqhost 镜像ID创建容器

在宿主机登录http://192.168.20.128:15672，用户名和密码均为guest

```

 ##### 开放端口

服务器！！！1

   

```cmd
 ### 完全开放端口
 firewall-cmd --zone=public --add-port=3306/tcp --permanent
###完全开放 端口
 firewall-cmd --reload

```


## hello world

### 生产者

```java
package com.rabbitmq.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.impl.AMQImpl;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 生产者
 */
public class Producer {
    public static void main(String[] args) {
         //所有的中间消息件都是基于 tcp ip 协议

         //创建工程连接
        ConnectionFactory connectionFactory=new ConnectionFactory();
        //连接获取通道
        connectionFactory.setHost("121.5.110.20");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("admin");
        connectionFactory.setVirtualHost("/");
    //通过创建交换机   声明队列 路由key 发送消息 和接收消息
        Connection connection=null;
        String querName="queue1";
        Channel channel=null;
        try {
             connection = connectionFactory.newConnection("生产者");
            channel= connection.createChannel(); //获得交换机

            //第一个是队列名  第二个是否持久化 第三个是否排他性（是否独立） 第四个是否自动删除  第五个是否带其他参数
            channel.queueDeclare(querName,false,false,false,null);

            //准备消息内容
            String message="hello wold";
            // 发送消息给队列
            channel.basicPublish("",querName,null,message.getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }finally {
            //关闭连接
            if (channel.isOpen()&&channel!=null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            if (connection.isOpen()&&connection!=null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //关闭通道
        }


    }
}
```

### 消费者

```java
package com.rabbitmq.simple;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消费者
 */
public class Consumer {
    public static void main(String[] args) {
        //所有的中间消息件都是基于 tcp ip 协议

        //创建工程连接
        ConnectionFactory connectionFactory=new ConnectionFactory();
        //连接获取通道
        connectionFactory.setHost("121.5.110.20");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("admin");
        connectionFactory.setVirtualHost("/");
        //通过创建交换机   声明队列 路由key 发送消息 和接收消息
        Connection connection=null;
        String querName="queue1";
        Channel channel=null;
        try {
            connection = connectionFactory.newConnection("生产者");
            channel= connection.createChannel(); //获得交换机

            channel.basicConsume("queue1", true, new DeliverCallback() {
                        @Override
                        public void handle(String s, Delivery delivery) throws IOException {
                            System.out.println("收到的信息是:" + new String(delivery.getBody(), "utf-8"));
                        }
                    },
                    new CancelCallback() {
                        @Override
                        public void handle(String s) throws IOException {
                            System.out.println("接受失败");
                        }
                    }
        );
            System.out.println("开始接受信息");
      System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }finally {
            //关闭连接
            if (channel.isOpen()&&channel!=null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            if (connection.isOpen()&&connection!=null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //关闭通道
        }
    }
}
```

![image-20210401204918929](image/image-20210401204918929.png)

# springboot

```pom
   <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-amqp</artifactId>
        </dependency>

```

###application.yml

```yaml
# 服务端口
server:
  port: 8080
spring:
  rabbitmq:
    username: admin
    password: admin
    virtual-host: /
    host: 121.5.110.20
    port: 5672
```

# fanout模式（广播模式）

```java
@Service
public class OrderSerivce {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void makeOrder(String  userid,String product,Integer num){
        // 1.根据商品id查询商品是否充足

        String orderId = UUID.randomUUID().toString();
        //2.保存订单
        //3.通rabbitmq 分发信息
        System.out.println("订单生成成功:" + orderId);
        //参数一 交换机 参数二是 路由key / 队列名参数三消息内容
        String exchangeName="fanout_order_exchange";
        String routingkey="";
        rabbitTemplate.convertAndSend(exchangeName,routingkey,orderId);

    }

}
```

###config - rabbitmq配置

```java
@Configuration
public class RabbitmqConfig {
          //1. 声明 注册fanout

        @Bean
     public FanoutExchange fanoutExchange(){
             //参数一 交换机名 参数二 持久化 参数三是否自动删除
            return new FanoutExchange("fanout_order_exchange",true,false);
        }
        //2. 声明队列sms.fanout.queue.email.fanout.queu.duanxin.fabout.queue
    @Bean
    public Queue smsqueue(){
            return new Queue("sms.fanout.queue",true);
    }
    @Bean
    public Queue duixinqueue(){
        return new Queue("duixin.fanout.queue",true);
    }
    @Bean
    public Queue emailqueue(){
        return new Queue("email.fanout.queue",true);
    }
        //3.完成绑定关系 队列和交换机 我擦绑定关系

     @Bean
    public Binding binding1(){
            return BindingBuilder.bind(smsqueue()).to(fanoutExchange());
     }
    @Bean
    public Binding binding2(){
        return BindingBuilder.bind(duixinqueue()).to(fanoutExchange());
    }
    @Bean
    public Binding binding3(){
        return BindingBuilder.bind(emailqueue()).to(fanoutExchange());
    }
}

```

###application.yml

```yaml
# 服务端口
server:
  port: 8080
spring:
  rabbitmq:
    username: admin
    password: admin
    virtual-host: /
    host: 121.5.110.20
    port: 5672
```

###消费者java代码

```java
//绑定 队列         绑定队列名
@Component
@RabbitListener(queues = {"email.fanout.queue"})
public class EmailConsumer {
    /**
     * 信息落脚点
     * @param message
     */
    @RabbitHandler
    public  void reviceMessage(String message){
        System.out.println("email.fanout.queue ----接收到了订单信息 "+message);
    }
}
```

![image-20210403183329766](image/image-20210403183329766.png)

 

##Dircet模式（直接交换模式）

###注册配置

```java
@Configuration
public class DircetRabbitmqConfig {
          //1. 声明 注册direct 模式

        @Bean
     public DirectExchange directExchange(){
             //参数一 交换机名 参数二 持久化 参数三是否自动删除
            return new DirectExchange("direct_order_exchange",true,false);
        }
        //2. 声明队列sms.direct.queue.email.fanout.queu.duanxin.fabout.queue
    @Bean
    public Queue Directsmsqueue(){
            return new Queue("sms.direct.queue",true);
    }
    @Bean
    public Queue Directduixinqueue(){
        return new Queue("duixin.direct.queue",true);
    }
    @Bean
    public Queue Directemailqueue(){
        return new Queue("email.direct.queue",true);
    }
        //3.完成绑定关系 队列和交换机 我擦绑定关系

     @Bean
    public Binding binding1(){
            return BindingBuilder.bind(Directsmsqueue()).to(directExchange()).with("sms"); //路由key
     }
    @Bean
    public Binding binding2(){
        return BindingBuilder.bind(Directduixinqueue()).to(directExchange()).with("duixin");
    }
    @Bean
    public Binding binding3(){
        return BindingBuilder.bind(Directemailqueue()).to(directExchange()).with("email");
    }
}
```

###接收配置

```java
//绑定 队列
@Component
@RabbitListener(queues = {"email.direct.queue"})
public class DircetEmailConsumer {
    /**
     * 信息落脚点
     * @param message
     */
    @RabbitHandler
    public  void reviceMessage(String message){
        System.out.println("email.direct.queue ----接收到了订单信息 "+message);
    }
}
```

###发送消息

```java
@Service
public class OrderSerivce {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void makeOrder(String  userid,String product,Integer num){
        // 1.根据商品id查询商品是否充足

        String orderId = UUID.randomUUID().toString();
        //2.保存订单
        //3.通rabbitmq 分发信息
        System.out.println("订单生成成功:" + orderId);
        //参数一 交换机 参数二是 路由key / 队列名参数三消息内容
        String exchangeName="direct_order_exchange";
        rabbitTemplate.convertAndSend(exchangeName,"sms",orderId);
        rabbitTemplate.convertAndSend(exchangeName,"duixin",orderId);
        rabbitTemplate.convertAndSend(exchangeName,"email",orderId);

    }

}
```

## Topic模式（主题模式)

###注解配置和接收

```java
//绑定 队列
@Component
@RabbitListener(bindings = @QueueBinding(value = @Queue(value = "email.topic.queue",declare = "true",autoDelete = "false"),
 exchange = @Exchange(value = "topic_order_exchange",type = ExchangeTypes.TOPIC),
        key = "*.email.#"
))
public class TopicEmailConsumer {
    /**
     * 信息落脚点
     * @param message
     */
    @RabbitHandler
    public  void reviceMessage(String message){
        System.out.println("email.topic.queue ----接收到了订单信息 "+message);
    }
}
```

### 发送信息

```java
/**
 * makeTopic
 * @param userid
 * @param product
 * @param num
 */
public void makeTopic(String  userid,String product,Integer num){
    // 1.根据商品id查询商品是否充足

    String orderId = UUID.randomUUID().toString();
    //2.保存订单
    //3.通rabbitmq 分发信息
    System.out.println("订单生成成功:" + orderId);
    //参数一 交换机 参数二是 路由key / 队列名参数三消息内容
    String exchangeName="topic_order_exchange";
    /**
     * #.duixin.#         #: 代表多级 任意都可
     * *.email.#         *: 代表必须一级
     * com.#
     */

    rabbitTemplate.convertAndSend(exchangeName,"com.email.duixin",orderId);

}
```

##过期时间

![image-20210404153823783](image/image-20210404153823783.png)

 ### 队列中设置过去时间和在消费设置过去时间的配置和注册

```java
@Configuration
public class TTLRabbitmq {
    //1. 声明 注册direct 模式

    @Bean
    public DirectExchange ttldirectExchange(){
        //参数一 交换机名 参数二 持久化 参数三是否自动删除
        return new DirectExchange("ttl_order_exchange",true,false);
    }
    //2. 声明队列sms
    @Bean
    public Queue ttlDirectsmsqueue(){
        //设置过期时间
        Map<String,Object> map=new HashMap<>();
        map.put("x-message-ttl",5000);//一定是int类型 设置5秒     ！！！ 在队列设置了过期时间就会专转移到死信队列当中
        return new Queue("ttl.direct.queue",true,false,false,map);
    }
    //交换机与队列绑定
    @Bean
    public Binding ttlbinding1(){
        return BindingBuilder.bind(ttlDirectsmsqueue()).to(ttldirectExchange()).with("ttl"); //路由key
    }

    //2. 声明队列tll
    @Bean
    public Queue ttlDirectsmsqueuemessge(){
        //设置过期时间
        return new Queue("ttl.messge.direct.queue",true);
    }
    @Bean
    public Binding ttlbindingmessgae(){
        return BindingBuilder.bind(ttlDirectsmsqueuemessge()).to(ttldirectExchange()).with("ttlmessage"); //路由key
    }
}
```

### 队列中设置过去时间和在消费设置过去时间的消费

```java
/**
 * makeTtl
 * @param userid
 * @param product
 * @param num
 */
public void makeTtl(String  userid,String product,Integer num){
    // 1.根据商品id查询商品是否充足

    String orderId = UUID.randomUUID().toString();
    //2.保存订单
    //3.通rabbitmq 分发信息
    System.out.println("订单生成成功:" + orderId);
    //参数一 交换机 参数二是 路由key / 队列名参数三消息内容
    String exchangeName="ttl_order_exchange";

    rabbitTemplate.convertAndSend(exchangeName,"ttl",orderId);

}

/**
 * makeTtlmessage
 * @param userid
 * @param product
 * @param num
 */
public void makeTtlmessage(String  userid,String product,Integer num){
    // 1.根据商品id查询商品是否充足

    String orderId = UUID.randomUUID().toString();
    //2.保存订单
    //3.通rabbitmq 分发信息
    System.out.println("订单生成成功:" + orderId);
    //参数一 交换机 参数二是 路由key / 队列名参数三消息内容
    String exchangeName="ttl_order_exchange";
    MessagePostProcessor postProcessor=new MessagePostProcessor() {
        @Override
        public Message postProcessMessage(Message message) throws AmqpException {
            message.getMessageProperties().setExpiration("5000"); //设置时长 5秒
            message.getMessageProperties().setContentEncoding("UTF-8");//设置编码
            return message;
        }
    };

    rabbitTemplate.convertAndSend(exchangeName,"ttlmessage",orderId,postProcessor);

}
```

##高级死信队列

过期了的信息就进入死信队列

###配置

```java
@Configuration
public class DeadRabbitmq {
    //1. 声明 注册direct 模式   死信队列

    @Bean
    public DirectExchange deadExchange(){
        //参数一 交换机名 参数二 持久化 参数三是否自动删除
        return new DirectExchange("dead_order_exchange",true,false);
    }
    //2. 声明队列sms
    @Bean
    public Queue deadDirectsmsqueue(){
        return new Queue("dead.direct.queue",true);
    }
    @Bean
    public Binding deadbindingmessgae(){
        return BindingBuilder.bind(deadDirectsmsqueue()).to(deadExchange()).with("dead"); //路由key
    }
}
```

```java
@Configuration
public class TTLRabbitmq {
    //1. 声明 注册direct 模式

    @Bean
    public DirectExchange ttldirectExchange(){
        //参数一 交换机名 参数二 持久化 参数三是否自动删除
        return new DirectExchange("ttl_order_exchange",true,false);
    }
    //2. 声明队列sms
    @Bean
    public Queue ttlDirectsmsqueue(){
        //设置过期时间
        Map<String,Object> map=new HashMap<>();
        map.put("x-message-ttl",5000);//一定是int类型 设置5秒     ！！！ 在队列设置了过期时间就会专转移到死信队列当中
        map.put("x-dead-letter-exchange","dead_order_exchange");//设置时间过期了进入到的死信队列
        map.put("x-dead-letter-routing-key","dead");  //设置路由key 有就设置
         //x-max-length  设置长度
        return new Queue("ttl.direct.queue",true,false,false,map);
    }
    @Bean
    public Binding ttlbinding1(){
        return BindingBuilder.bind(ttlDirectsmsqueue()).to(ttldirectExchange()).with("ttl"); //路由key
    }

    //2. 声明队列tll
    @Bean
    public Queue ttlDirectsmsqueuemessge(){
        //设置过期时间
        return new Queue("ttl.messge.direct.queue",true);
    }
    @Bean
    public Binding ttlbindingmessgae(){
        return BindingBuilder.bind(ttlDirectsmsqueuemessge()).to(ttldirectExchange()).with("ttlmessage"); //路由key
    }
}
```

# Rabbitmq的docker集群搭建

```dockerfile
 #创建第一个rabbitmq的服务
docker run -d --hostname rabbit1 --name myrabbit1 -p 15672:15672 -p 5672:5672 -e RABBITMQ_ERLANG_COOKIE='rabbitcookie' rabbitmq:3.7.7-management
 
 #创建第二个rabbitmq的服务 
docker run -d --hostname rabbit2 --name myrabbit2 -p 5673:5672 --link myrabbit1:rabbit1 -e RABBITMQ_ERLANG_COOKIE='rabbitcookie' rabbitmq:3.7.7-management


 # 设置节点1：  
  docker exec -it myrabbit1 bash
  rabbitmqctl stop_app
  rabbitmqctl reset
  rabbitmqctl start_app
  exit
  
#  设置节点2，加入到集群：
  docker exec -it myrabbit2 bash
  rabbitmqctl stop_app
  rabbitmqctl reset
  rabbitmqctl join_cluster --ram rabbit@rabbit1
  rabbitmqctl start_app
  exit
```





